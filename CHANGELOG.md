## 1.2.0

- feat: adds `terraform/module-aws-dynamodb`

## 1.1.2

- fix: approvals default setup was not properly applied

## 1.1.1

- test: fixes run of tagged pipeline with insufficient permissions

## 1.1.0

- feat: protects semver tags everywhere
- feat: hardens MR reviews
- feat: protects tags

## 1.0.0

- feat: (BREAKING) requires gitlab provider 17+

## 0.10.0

- feat: adds `module-aws-opensearch`

## 0.9.0

- maintenance: updates gitlab group and project modules
- maintenance: requires gitlab provider 16+

## 0.8.0

- feat: adds `module-aws-ses`
- refactor: make CHANGELOG consistent with other in this project
- chore: bump pre-commit hooks

## 0.7.0

- feat: removes duplicated `module-aws-cloudfront` configuration

## 0.6.0

- feat: adds `module-aws-cloudfront`
- chore: bump pre-commit hooks

## 0.5.3

- refactor: removes `lookup`s without alternative values

## 0.5.2

- feat: adds `module-aws-load-balancer`

## 0.5.1

- fix: enable container registry for kind image

## 0.5.0

- feat: adds terraform `module-aws-cloudfront`

## 0.4.0

- feat: update gitlab groups/projects with new features
- feat: forces OTP to access groups
- feat: forces `only_allow_merge_if_pipeline_succeeds` to `true`
- maintenance: updates Terraform code to comply with tf 1.3+
- maintenance: pre-commit dependencies updates

## 0.3.0

- feat: (Terraform) add repository `module-nexus-user`
- chore: use `https` instead of `git` protocol for pre-commit hooks
- chore: bump pre-commit hooks

## 0.2.0

- feat: add docker jenkins master repo

## 0.1.0

- feat: add new repos concerning ECS

## 0.0.1

- feat: add variable to enable or not container registry on repo

## 0.0.0

- tech: initialise repository
