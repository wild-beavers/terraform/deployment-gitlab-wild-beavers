locals {
  topics = []
  default_labels = {
    "critical bug::critical" : {
      description = "Bug that impairs the main features of the tool; Must be fixes as soon as possible"
      color       = "#FF0000"
    },
    "bug::high" : {
      description = "Generic bug in the main features of the tool, with a known/standard way to fix it"
      color       = "#FF0000"
    },
    "edge bug::medium" : {
      description = "Bug on a side; not frequently used feature"
      color       = "#c39953"
    },
    "heisenbug::medium" : {
      description = "Bug that seems to disappear in an attempt to study it"
      color       = "#c39953"
    },
    "chore" : {
      description = "Issue that requires some basic maintenance of the code: pins new version, dependency updates, etc"
      color       = "#5843AD"
    },
    "doc" : {
      description = "Issue that requires editing or adding new documentation"
      color       = "#5843AD"
    },
    "enhancement" : {
      description = "Issue that ask for a new feature"
      color       = "#5843AD"
    },
    "need more info" : {
      description = "Issue that require the input of a user before taking any action"
      color       = "#6699cc"
    },
    "to describe" : {
      description = "Issue not sufficiently described. All the necessary information is known, but the issue itself lacks written information."
      color       = "#6699cc"
    },
    "to sort" : {
      description = "Issue that need some labeling."
      color       = "#6699cc"
    }
  }
}

data "gitlab_current_user" "current" {}

module "group" {
  source  = "gitlab.com/wild-beavers/module-gitlab-group/gitlab"
  version = "~> 3.0"

  for_each = local.groups

  name = each.key
  path = each.value.path

  auto_devops_enabled                = lookup(each.value, "auto_devops_enabled", false)
  avatar                             = lookup(each.value, "avatar", format("files/avatar/%s-group.png", each.key))
  default_branch_protection_defaults = lookup(each.value, "default_branch_protection", null)
  description                        = each.value.description
  emails_enabled                     = lookup(each.value, "emails_enabled", true)
  group_memberships                  = lookup(each.value, "group_memberships", {})
  labels                             = lookup(each.value, "labels", local.default_labels)
  lfs_enabled                        = lookup(each.value, "lfs_enabled", false)
  mentions_disabled                  = lookup(each.value, "mentions_disabled ", false)
  parent_id                          = lookup(each.value, "parent_id", null)
  project_creation_level             = lookup(each.value, "project_creation_level", "maintainer")
  request_access_enabled             = lookup(each.value, "request_access_enabled", false)
  require_two_factor_authentication  = lookup(each.value, "require_two_factor_authentication", true)
  subgroup_creation_level            = lookup(each.value, "subgroup_creation_level", "owner")
  two_factor_grace_period            = lookup(each.value, "two_factor_grace_period", 336)
  visibility_level                   = lookup(each.value, "visibility_level", "public")
}

module "project" {
  source  = "gitlab.com/wild-beavers/module-gitlab-project/gitlab"
  version = "~> 5.0"

  for_each = {
    for key, project in merge(
      local.terraform_projects,
      local.container_projects,
      local.pipelines_projects,
      local.templates_projects,
      local.documentation_projects,
      ) : key => merge(project, {
        template_is_set = lookup(project, "template_name", null) != null || lookup(project, "default_template", true)
        template_name   = lookup(project, "default_template", true) ? format("%s", project.group_key) : null
    })
  }

  namespace_id = each.value.group_id

  name = each.key

  container_expiration_policy = {
    keep_n            = 50
    name_regex_keep   = "^[0-9]([0-9\\.\\-]+[0-9])?$"
    name_regex_delete = ".*"
    older_than        = "7d"
  }

  merge_commit_template = "Please, do not use merge commits."
  level_mr_approvals = {
    disable_overriding_approvers_per_merge_request = true
    require_password_to_approve                    = true
    reset_approvals_on_push                        = true
  }
  project_approval_rules = {
    base = {
      name                              = "base"
      approvals_required                = 2
      applies_to_all_protected_branches = true
      rule_type                         = "any_approver"
    }
  }

  avatar                              = lookup(each.value, "avatar", format("files/avatar/%s.png", lookup(each.value, "topics", local.topics)[0]))
  description                         = each.value.description
  analytics_access_level              = lookup(each.value, "analytics_access_level", "private")
  import_url                          = lookup(each.value, "import_url", null)
  branch_protection_push_access_level = lookup(each.value, "branch_protection_push_access_level", "no one")
  push_rules = {
    deny_delete_tag = lookup(each.value, "push_rule_deny_delete_tag", false)
  }
  container_registry_access_level       = lookup(each.value, "container_registry_access_level", "disabled")
  pages_access_level                    = lookup(each.value, "pages_access_level", "disabled")
  only_allow_merge_if_pipeline_succeeds = lookup(each.value, "only_allow_merge_if_pipeline_succeeds", true)
  issues_access_level                   = lookup(each.value, "issues_access_level", "enabled")
  squash_option                         = lookup(each.value, "squash_option", "never")
  shared_runners_enabled                = lookup(each.value, "shared_runners_enabled", true)
  infrastructure_access_level           = lookup(each.value, "infrastructure_access_level", "disabled")
  packages_enabled                      = lookup(each.value, "packages_enabled", false)

  tag_protections = {
    default_branch = {
      tag                 = "master"
      create_access_level = "no one"
    }
    semver = {
      tag                 = "*.*.*"
      create_access_level = "no one"
      user_id             = data.gitlab_current_user.current.id
    }
  }

  group_with_project_templates_id = each.value.template_is_set ? module.group["templates"].id : null
  use_custom_template             = each.value.template_is_set
  template_name                   = each.value.template_name
}
