# Contributing

First of all, thank you for contributing to this repository.

## Tooling

This is a list of tools that you should have to contribute to this repository:

* [pre-commit](https://pre-commit.com/)
* [tfsec](https://github.com/aquasecurity/tfsec)
* [terraform](https://www.terraform.io/)
* [tflint](https://github.com/terraform-linters/tflint)
* [terraform-docs](https://github.com/terraform-docs/terraform-docs)

## Versioning

This repository doesn't use versioning, we automatically deploy the master branch.

## Commits

We follow [conventional commits](https://www.conventionalcommits.org/).
