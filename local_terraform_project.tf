locals {
  terraform_projects = {
    deployment-gitlab-wild-beavers = merge(
      local.terraform_defaults,
      {
        group_key                   = "terraform-deployment"
        description                 = "Deployment project to setup gitlab repositories, settings and groups."
        topics                      = concat(local.terraform_deployment_topics, ["gitlab"]),
        avatar                      = "files/avatar/git.png"
        default_template            = false
        infrastructure_access_level = "private"
      }
    )

    # AWS
    module-aws-backup = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to use AWS Backup."
        topics           = concat(local.terraform_module_topics, ["aws", "backup"])
        default_template = false
      }
    )
    module-aws-batch = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an AWS Batch compute environment."
        topics           = concat(local.terraform_module_topics, ["aws", "batch"])
        default_template = false
      }
    )
    module-aws-batch-job = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an AWS Batch job."
        topics           = concat(local.terraform_module_topics, ["aws", "batch"])
        default_template = false
      }
    )
    module-aws-bootstrap = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create S3 bucket for Terraform state files."
        topics           = concat(local.terraform_module_topics, ["aws", "s3"])
        default_template = false
      }
    )
    module-aws-bucket-s3 = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create S3 Buckets."
        topics           = concat(local.terraform_module_topics, ["aws", "s3"])
        default_template = false
      }
    )
    module-aws-codebuild = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to CodeBuild projects."
        topics           = concat(local.terraform_module_topics, ["aws", "codebuild"])
        default_template = false
      }
    )
    module-aws-cloudwatch-log-group = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an CloudWatch Log Group."
        topics           = concat(local.terraform_module_topics, ["aws", "cloudwatch"])
        default_template = false
      }
    )
    module-aws-cost-optimization = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to enable cost optimizations on AWS."
        topics           = concat(local.terraform_module_topics, ["aws", "cost-optimizations"])
        default_template = false
      }
    )
    module-aws-dynamodb = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to use AWS Dynamo DB."
        topics           = concat(local.terraform_module_topics, ["aws", "dynamodb"])
        default_template = true
      }
    )
    module-aws-efs = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an EFS."
        topics           = concat(local.terraform_module_topics, ["aws", "EFS"])
        default_template = false
      }
    )
    module-aws-kms = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to manage KMS."
        topics           = concat(local.terraform_module_topics, ["aws", "KMS"])
        default_template = false
      }
    )
    module-aws-eks = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an EKS cluster."
        topics           = concat(local.terraform_module_topics, ["aws", "eks"])
        default_template = false
      }
    )
    module-aws-ecr = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create ECR."
        topics           = concat(local.terraform_module_topics, ["aws", "ecr"])
        default_template = false
      }
    )
    module-aws-iam-group = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an IAM group with associated role."
        topics           = concat(local.terraform_module_topics, ["aws", "iam"])
        default_template = false
      }
    )
    module-aws-eks-service-account-role = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an EKS Service Account linked to an IAM Role."
        topics           = concat(local.terraform_module_topics, ["aws", "eks", "iam"])
        default_template = false
      }
    )
    module-aws-eks-worker-pool = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an EKS worker pool."
        topics           = concat(local.terraform_module_topics, ["aws", "eks", "asg"])
        default_template = false
      }
    )
    module-aws-fargate = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to use fargate."
        topics           = concat(local.terraform_module_topics, ["aws", "fargate"])
        default_template = false
      }
    )
    module-aws-organization = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to manage an AWS Organization."
        topics           = concat(local.terraform_module_topics, ["aws", "organization"])
        default_template = false
      }
    )
    module-aws-route53 = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to use AWS Route53."
        topics           = concat(local.terraform_module_topics, ["aws", "route53"])
        default_template = false
      }
    )
    module-aws-secret-manager = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create Secret Manager resources."
        topics           = concat(local.terraform_module_topics, ["aws", "secret_manager"])
        default_template = false
      }
    )
    module-aws-service-catalog-portfolio = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create a service-catalog portfolio."
        topics           = concat(local.terraform_module_topics, ["aws", "service-catalog"])
        default_template = false
      }
    )
    module-aws-service-catalog-product = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create a service-catalog product."
        topics           = concat(local.terraform_module_topics, ["aws", "service-catalog"])
        default_template = false
      }
    )
    module-aws-service-user = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create a service user."
        topics           = concat(local.terraform_module_topics, ["aws"])
        default_template = false
      }
    )
    module-aws-sg-rules-active-directory = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create an SG rules for Microsoft Active Directory."
        topics           = concat(local.terraform_module_topics, ["aws", "sg"])
        default_template = false
      }
    )
    module-aws-ssm-documents = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create SSM documents."
        topics           = concat(local.terraform_module_topics, ["aws", "ssm"])
        default_template = false
      }
    )
    module-aws-ssm-parameters = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create SSM parameters."
        topics           = concat(local.terraform_module_topics, ["aws", "ssm"])
        default_template = false
      }
    )
    module-aws-transit-gateway = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create a transit gateway."
        topics           = concat(local.terraform_module_topics, ["aws", "transit-gateway"])
        default_template = false
      }
    )
    module-aws-terraform-baseline = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create associated resources helpful for Terraform."
        topics           = concat(local.terraform_module_topics, ["aws", "iam"])
        default_template = false
      }
    )
    module-aws-virtual-machine = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create a virtual machine."
        topics           = concat(local.terraform_module_topics, ["aws", "ec2"])
        default_template = false
      }
    )
    module-aws-ecs = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create ECS resources."
        topics           = concat(local.terraform_module_topics, ["aws", "ecs"])
        default_template = false
      }
    )
    module-aws-cloudfront = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create Cloudfront resources."
        topics           = concat(local.terraform_module_topics, ["aws", "cloudfront"])
        default_template = false
      }
    )
    module-aws-load-balancer = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create AWS Load Balancer."
        topics           = concat(local.terraform_module_topics, ["aws", "ELB"])
        default_template = true
      }
    )
    module-aws-ses = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create AWS SES resources."
        topics           = concat(local.terraform_module_topics, ["aws", "ses"])
        default_template = true
      }
    )
    module-aws-iam-policy = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create AWS Policies internal and external."
        topics           = concat(local.terraform_module_topics, ["aws", "IAM"])
        default_template = true
      }
    )
    module-aws-opensearch = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create AWS Opensearch resources."
        topics           = concat(local.terraform_module_topics, ["aws", "opensearch"])
        default_template = true
      }
    )

    # Bitbucket
    module-bitbucket-repository = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to create bitbucket repositories with branch protection."
        topics           = concat(local.terraform_module_topics, ["bitbucket", "repository"])
        default_template = false
      }
    )

    # Gitlab
    module-gitlab-group = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to manager gitlab groups"
        topics           = concat(local.terraform_module_topics, ["gitlab", "group"])
        default_template = false
      }
    )
    module-gitlab-project = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to manager gitlab projects."
        topics           = concat(local.terraform_module_topics, ["gitlab", "project"])
        default_template = false
      }
    )

    # Kubernetes
    module-kubernetes-anchore = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy anchore on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "aws-health-exporter"])
        default_template = false
      }
    )
    module-kubernetes-aws-health-exporter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy aws-health-exporter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "aws-health-exporter"])
        default_template = false
      }
    )
    module-kubernetes-azure-metrics-exporter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy azure-metrics-exporter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "azure-metrics-exporter"])
        default_template = false
      }
    )
    module-kubernetes-blackbox-exporter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy blackbox-exporter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "blackbox-exporter"])
        default_template = false
      }
    )
    module-kubernetes-cloudwatch-agent = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy cloudwatch-agent on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "cloudwatch-agent"])
        default_template = false
      }
    )
    module-kubernetes-cloudwatch-agent-prometheus = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy the prometheus version of cloudwatch-agent on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "cloudwatch-agent", "prometheus"])
        default_template = false
      }
    )
    module-kubernetes-cloudwatch-exporter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy cloudwatch-exporter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "cloudwatch-exporter"])
        default_template = false
      }
    )
    module-kubernetes-cluster-autoscaler = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy cluster-autoscaler on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "cluster-autoscaler"])
        default_template = false
      }
    )
    module-kubernetes-grafana = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy grafana on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "grafana"])
        default_template = false
      }
    )
    module-kubernetes-jenkins = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy jenkins on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "jenkins"])
        default_template = false
      }
    )
    module-kubernetes-jmeter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy jmeter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "jmeter"])
        default_template = false
      }
    )
    module-kubernetes-keycloak-gatekeeper = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy keycloak-gatekeeper on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "keycloak-gatekeeper"])
        default_template = false
      }
    )
    module-kubernetes-kube-state-metrics = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy kube-state-metrics on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "kube-state-metrics"])
        default_template = false
      }
    )
    module-kubernetes-lighthouse-exporter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy lighthouse-exporter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "lighthouse-exporter", "prometheus"])
        default_template = false
      }
    )
    module-kubernetes-nginx-ingress-controller = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy nginx-ingress-controller on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "nginx-ingress-controller"])
        default_template = false
      }
    )
    module-kubernetes-node-exporter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy node-exporter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "node-exporter"])
        default_template = false
      }
    )
    module-kubernetes-nexus3 = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy nexus3 on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "nexus3"])
        default_template = false
      }
    )
    module-kubernetes-nvidia-device-plugin = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy nvidia-device-plugin on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "nvidia"])
        default_template = false
      }
    )
    module-kubernetes-oracledb-exporter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy oracledb-exporter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "oracledb-exporter"])
        default_template = false
      }
    )
    module-kubernetes-prometheus = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy Prometheus on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "prometheus"])
        default_template = false
      }
    )
    module-kubernetes-redis = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy redis on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "redis"])
        default_template = false
      }
    )
    module-kubernetes-redis-exporter = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy redis-exporter on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "redis-exporter"])
        default_template = false
      }
    )
    module-kubernetes-renovate = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy renovate on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "renovate"])
        default_template = false
      }
    )
    module-kubernetes-runscope-radar = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy runscope-radar on kubernetes."
        topics           = concat(local.terraform_module_topics, ["kubernetes", "runscope-radar"])
        default_template = false
      }
    )

    # Nexus
    module-nexus-user = merge(
      local.terraform_defaults,
      {
        description      = "Terraform module to deploy Nexus users."
        topics           = concat(local.terraform_module_topics, ["nexus"])
        default_template = false
      }
    )
  }

  terraform_topics            = ["terraform"]
  terraform_deployment_topics = concat(local.terraform_topics, ["deployment"])
  terraform_module_topics     = concat(local.terraform_topics, ["module"])
  terraform_defaults = {
    group_id         = module.group["terraform"].id
    group_key        = "terraform-module"
    packages_enabled = true
  }
}
