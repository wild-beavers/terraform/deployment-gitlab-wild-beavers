locals {
  container_projects = {
    aws-cli = merge(
      local.container_defaults,
      {
        description      = "AWS-CLI container image build files."
        topics           = concat(local.container_topics, ["aws-cli"]),
        default_template = false
      }
    )
    aws-efs-mount = merge(
      local.container_defaults,
      {
        description      = "AWS EFS mount command container image build files."
        topics           = concat(local.container_topics, ["aws-efs"]),
        default_template = false
      }
    )
    kind = merge(
      local.container_defaults,
      {
        description      = "KinD container image build files."
        topics           = concat(local.container_topics, ["kind"]),
        default_template = false
      }
    )
    lighthouse-exporter = merge(
      local.container_defaults,
      {
        description      = "Container used to run prometheus lighthouse-exporter."
        topics           = concat(local.container_topics, ["lighthouse-exporter", "prometheus", "exporter"]),
        default_template = false
      }
    )
    pre-commit = merge(
      local.container_defaults,
      {
        description      = "pre-commit container image build files."
        topics           = concat(local.container_topics, ["pre-commit"]),
        default_template = false
      }
    )
    relint = merge(
      local.container_defaults,
      {
        description      = "Relint in a container image."
        topics           = concat(local.container_topics, ["relint"]),
        default_template = false
      }
    )
    jenkins-master = merge(
      local.container_defaults,
      {
        description      = "Jenkins master container image with docker cli."
        topics           = concat(local.container_topics, ["jenkins"]),
        default_template = false
      }
    )
  }
  container_topics = ["docker", "container"]
  container_defaults = {
    group_id                        = module.group["docker"].id
    group_key                       = "docker"
    container_registry_access_level = "enabled"
    avatar                          = "files/avatar/container.png"
  }
}
