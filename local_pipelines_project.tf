locals {
  pipelines_projects = {
    gitlab-ci : {
      group_key          = "groovy"
      description        = "Library of gitlab-ci pipelines and stages"
      topics             = concat(local.pipelines_topics, ["gitlab-ci"]),
      group_id           = module.group["pipelines"].id
      pages_access_level = "enabled"
      avatar             = "files/avatar/gitlab.png"
      default_template   = false
    }
  }
  pipelines_topics = ["pipelines"]
}
