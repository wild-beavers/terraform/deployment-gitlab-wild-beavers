locals {
  groups = {
    terraform = {
      path                    = "terraform"
      description             = "Terraform modules"
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
    pipelines = {
      path                    = "pipelines"
      description             = "CI/CD pipeline code. Jenkins pipeline. Gitlab pipeline. etc."
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
    docker = {
      path                    = "docker"
      description             = "Container OCI code."
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
    documentation = {
      path                    = "documentation"
      description             = "Documentation."
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
    templates = {
      path                    = "templates"
      description             = "Reusable project templates"
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
  }
}
