#####
# Groups
#####

output "groups" {
  value     = { for k, v in module.group : k => v }
  sensitive = true
}

#####
# Projects
#####

output "projects" {
  value = { for k, v in module.project : k => { for i, j in v : i => j if !contains(["runners_token", "access_tokens", "access_tokens_user_tokens"], i) } }
}
