locals {
  documentation_projects = {
    static-analysis-configuration : {
      group_key          = "doc"
      description        = "Confifurations for various static analysis tools for code guidelines compliance."
      topics             = concat(local.documentation_topics, ["linter", "configuration"]),
      group_id           = module.group["documentation"].id
      pages_access_level = "enabled"
      avatar             = "files/avatar/documentation.png"
      default_template   = false
    }
  }
  documentation_topics = ["documentation"]
}
