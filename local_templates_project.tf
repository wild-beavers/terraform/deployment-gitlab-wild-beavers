locals {
  templates_projects = {
    terraform-module : {
      description                         = "Template to initiate a new terraform module."
      topics                              = concat(local.templates_topics, ["terraform", "module"])
      group_id                            = module.group["templates"].id
      branch_protection_push_access_level = "maintainer"
      default_template                    = false
    }
    terraform-deployment : {
      description                         = "Template to initiate a new terraform deployment."
      topics                              = concat(local.templates_topics, ["terraform", "module"])
      group_id                            = module.group["templates"].id
      branch_protection_push_access_level = "maintainer"
      default_template                    = false
    }
    container : {
      description                         = "Template to initiate a new container OCI code."
      topics                              = concat(local.templates_topics, ["container", "oci", "docker", "podman"])
      group_id                            = module.group["templates"].id
      branch_protection_push_access_level = "maintainer"
      container_registry_access_level     = "enabled"
      default_template                    = false
    }
    butane : {
      description                         = "Template to initiate a new butane/ignitions projects."
      topics                              = concat(local.templates_topics, ["butane", "fcos", "ignition"])
      group_id                            = module.group["templates"].id
      branch_protection_push_access_level = "maintainer"
      default_template                    = false
    }
    bash : {
      description                         = "Template to initiate a new bash project."
      topics                              = concat(local.templates_topics, ["bash"])
      group_id                            = module.group["templates"].id
      branch_protection_push_access_level = "maintainer"
      default_template                    = false
    }
    groovy : {
      description                         = "Template to initiate a new groovy project."
      topics                              = concat(local.templates_topics, ["groovy"])
      group_id                            = module.group["templates"].id
      branch_protection_push_access_level = "maintainer"
      default_template                    = false
    }
    python : {
      description                         = "Template to initiate a new python project."
      topics                              = concat(local.templates_topics, ["python"])
      group_id                            = module.group["templates"].id
      branch_protection_push_access_level = "maintainer"
      default_template                    = false
    }
  }
  templates_topics = ["templates"]
}
